import express from 'express'

const app = express();

import api from './routes/api.js';
import login from './login.js';
import signup from './signup.js'

app.use(express.json())

app.use('/api', api);

app.post('/credentials', signup)

app.put('/credentials', login)

const listener = app.listen(8080, () => {
    console.log(`Express draait op poort ${listener.address().port}`)
});