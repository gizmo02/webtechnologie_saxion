import express from 'express';
import users from './users.js';
import products from './products.js';
import status from 'http-status-codes';
import { verifyToken } from '../util.js';
import {category} from "../model/category_model.js";

const { UNAUTHORIZED } = status
const router = express.Router();

// voeg routes toe
router.use('/users', users)

router.use('/products', products)


export default router;