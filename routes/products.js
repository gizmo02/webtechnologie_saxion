import express from 'express';
import status from 'http-status-codes'
import bodyParser from "body-parser";
import {product} from '../model/product_model.js'
import {category} from "../model/category_model.js";

const router = new express.Router();

router.use(bodyParser.json());

// Get all the products
router.get('/', (req, res) => {
    res.json(product)
})

//Create a new product
router.post('/', (req, res) => {
    product.push({
        itemId: (product.length-1) + 1,
        category: category[req.body.category],
        name: req.body.name,
        price: req.body.price,
        bidded: false,
        timeLeft: "1 hour!"

    })
})

// Update a product
router.put('/', (req, res) => {

})

// Delete a product
router.delete('/', (req, res) => {

})

export default router;
