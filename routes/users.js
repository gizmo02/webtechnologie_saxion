import express from 'express';
import status from 'http-status-codes'
import bodyParser from "body-parser";
import bcrypt from 'bcryptjs';
import {users} from '../model/user_model.js'
import {verifyToken} from "../util.js";

const router = new express.Router();

router.use(bodyParser.json());

const { NOT_FOUND, UNAUTHORIZED } = status

router.use((req, res, next) => {
    try {
        const payload = verifyToken(req.header('Authorization'))
        if (payload) {
            req.token = payload
            return next()
        }
    } catch (problem) {

    }
    res.status(UNAUTHORIZED).end('Missing valid JSON web token')
})

// voeg routes toe
router.get('/', (req, res) => {
    if(req.token.roles.includes('admin')) {
        res.json(users)
    } else {
        res.status(UNAUTHORIZED).end('You have no admin privileges!')
    }
})

router.get('/:email', (req, res) => {
    const email = req.params.email
    const user = users.find(candidate => candidate.email === email)
    if(user !== undefined) {
        res.json(user)
    } else {
        res.status(NOT_FOUND).end('User does not exist')
    }
})

export default router;

export { users }

export function getUser(email) {
    return users.find(candidate => candidate.email === email)
}