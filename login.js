import status from 'http-status-codes'
import {checkPassword, createToken, hashPassword} from './util.js'
import { getUser } from './routes/users.js'

const {FORBIDDEN} = status


export default function (req, res) {
    const user = getUser(req.body?.email)
    const hashedPassword = hashPassword(user.plainPassword)
    if (!user || !checkPassword(req.body.password, hashedPassword)) {
        res.status(FORBIDDEN).end('Invalid email/password combination')
    } else {
            const payload = {
                user: user.email,
                roles: user.roles
            }
            res.json({token: createToken(payload)})
    }
}