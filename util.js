import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'

export function hashPassword(plainPassword) {
    return bcrypt.hashSync(plainPassword, bcrypt.genSaltSync(10))
}

export function checkPassword(plainPassword, hash) {
    return bcrypt.compareSync(plainPassword, hash)
}

const mySecret = 'My Secret on the server side ', bearerPrefix = 'Bearer '

export function createToken(payload) {
    return jwt.sign(payload, mySecret)
}

export function verifyToken(authorization) {
    return authorization.startsWith(bearerPrefix) && jwt.verify(authorization.substr(bearerPrefix.length), mySecret)
}