const users = [
    {
        name: 'Jasper',
        email: 'home@home.nl',
        plainPassword: 'passw0rd',
        roles: ['admin', 'user']
    },
    {
        name: 'Rick',
        email: 'away@home.nl',
        plainPassword: 'wrong',
        roles: ['user']
    }
]

export { users }