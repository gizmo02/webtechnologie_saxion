import express from 'express';
import status from 'http-status-codes'
import bodyParser from "body-parser";
import bcrypt from 'bcryptjs';
import {users} from "./model/user_model.js";


export default function (req, res) {
    const email = req.body?.email
    const user = users.find(candidate => candidate.email === email)
            if (user) {
                return res.status(409).json({
                    message: 'Account with this email already exists'
                })
            } else {
                users.push({
                    name: req.body.name,
                    email: req.body.email,
                    plainPassword: req.body.password,
                    roles: req.body.roles
                })
                return res.status(309).json({
                    message: 'Account created!'
                })
            }



}


